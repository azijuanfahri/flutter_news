import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../common/config.dart';
import '../../../models/app.dart';
import '../../../models/blog_news.dart';
import '../../../screens/blogs.dart';
import '../header/header_view.dart';

class BlogListItems extends StatefulWidget {
  final config;

  BlogListItems({this.config});

  @override
  _BlogListItemsState createState() => _BlogListItemsState();
}

class _BlogListItemsState extends State<BlogListItems> {
  Future<List<BlogNews>> _fetchBlogs;

  @override
  void initState() {
    _fetchBlogs = getBlogs(); // only create the future once.
    super.initState();
  }

  Future<List<BlogNews>> getBlogs() async {
    List<BlogNews> blogs = [];
    var _jsons = await BlogNews.getBlogs(url: serverConfig['url'], page: 1);
//    Provider.of<AppModel>(context).appConfig

    for (var item in _jsons) {
      blogs.add(BlogNews.fromJson(item));
    }
    return blogs;
  }

  Widget _buildHeader(context, blogs) {
    final locale = Provider.of<AppModel>(context).locale;
    if (widget.config.containsKey("name")) {
      var showSeeAllLink = widget.config['layout'] != "instagram";
      return HeaderView(
        headerText: widget.config["name"][locale] ?? '',
        showSeeAll: showSeeAllLink,
        callback: () => {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => BlogScreen(blogs: blogs),
            ),
          )
        },
      );
    }
    return Container();
  }

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;
//    var emptyPosts = [Blog.empty(1), Blog.empty(2), Blog.empty(3)];

    return Column(
      children: <Widget>[
        FutureBuilder<List<BlogNews>>(
          future: _fetchBlogs,
          builder: (context, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
              case ConnectionState.active:
              case ConnectionState.waiting:
                return Column(
                  children: <Widget>[
                    _buildHeader(context, null),
//                    BlogNewsView(posts: emptyPosts, index: 0),
//                    BlogNewsView(posts: emptyPosts, index: 1),
//                    BlogNewsView(posts: emptyPosts, index: 2),
                  ],
                );
                break;
              case ConnectionState.done:
              default:
                if (snapshot.hasError) {
                  return Container();
                } else {
                  List<BlogNews> blogs = snapshot.data;
                  print(blogs);
                  int length = blogs.length;
                  return Column(
                    children: <Widget>[
                      _buildHeader(context, blogs),
                      Container(
                        width: screenWidth,
                        height: screenWidth * 0.7,
                        child: PageView(
                          children: [
                            for (var i = 0; i < length; i = i + 3)
                              Column(
                                children: <Widget>[
//                                  if (blogs[i] != null)
//                                    BlogNewsView(posts: blogs, index: i),
//                                  if (i + 1 < length)
//                                    BlogNewsView(posts: blogs, index: i + 1),
//                                  if (i + 2 < length)
//                                    BlogNewsView(posts: blogs, index: i + 2),
                                ],
                              )
                          ],
                        ),
                      ),
                    ],
                  );
                }
            }
          },
        ),
      ],
    );
  }
}
