import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../common/tools.dart';
import '../../../models/app.dart';
import '../../../models/blog_news.dart';
import '../../../models/recent_product.dart';
import '../../../services/wordpress.dart';
import '../../../widgets/blog/blog_staggered.dart';
import '../../../widgets/blog/detailed_blog/blog_view.dart';
import '../blog_card_view.dart';
import '../header/header_view.dart';

class BlogListLayout extends StatefulWidget {
  final config;

  BlogListLayout({this.config});

  @override
  _ProductListItemsState createState() => _ProductListItemsState();
}

class _ProductListItemsState extends State<BlogListLayout> {
  final WordPress _service = WordPress();
  Future<List<BlogNews>> _getBlogsLayout;

  final _memoizer = AsyncMemoizer<List<BlogNews>>();

  @override
  void initState() {
    // only create the future once
    Future.delayed(Duration.zero, () {
      setState(() {
        _getBlogsLayout = getBlogsLayout(context);
      });
    });
    super.initState();
  }

  double _buildProductWidth(screenWidth) {
    switch (widget.config["layout"]) {
      case "twoColumn":
        return screenWidth * 0.5;
      case "threeColumn":
        return screenWidth * 0.35;
      case "fourColumn":
        return screenWidth / 4;
      case "recentView":
        return screenWidth / 4;
      case "card":
      default:
        return screenWidth - 10;
    }
  }

  double _buildProductHeight(screenWidth, isTablet) {
    switch (widget.config["layout"]) {
      case "twoColumn":
      case "threeColumn":
      case "fourColumn":
      case "recentView":
        return screenWidth * 0.5;
        break;
      case "card":
      default:
        var cardHeight = widget.config["height"] != null
            ? widget.config["height"] + 40.0
            : screenWidth * 1.4;
        return isTablet ? screenWidth * 1.3 : cardHeight;
        break;
    }
  }

  Future<List<BlogNews>> getBlogsLayout(context) async {
//    if (widget.config["layout"] == "recentView")
//      return Provider.of<RecentModel>(context).products;
    return _memoizer.runOnce(
      () => _service.fetchBlogLayout(
          config: widget.config,
          lang: Provider.of<AppModel>(context, listen: false).locale),
    );
  }

  Widget getBlogsListWidgets(List<BlogNews> products) {
    final screenSize = MediaQuery.of(context).size;
    final isTablet = Tools.isTablet(MediaQuery.of(context));

    return Container(
      constraints: BoxConstraints(
        minHeight: _buildProductHeight(screenSize.width, isTablet),
      ),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: List.generate(
            products.length,
            (index) {
              return Container(
                width:
                    _buildProductWidth(MediaQuery.of(context).size.width + 20),
                child: BlogCardView(
                  blogs: products,
                  index: index,
                ),
              );
            },
          ),

//            SizedBox(width: 15.0),
//            for (var item in products)
//              BlogCard(
//                item: item,
//                //isHero: true,
//                width: _buildProductWidth(screenSize.width),
//              ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final isTablet = Tools.isTablet(MediaQuery.of(context));
    final recentProduct = Provider.of<RecentModel>(context).products;
    final isRecent = widget.config["layout"] == "recentView" ? true : false;

    if (isRecent && recentProduct.length < 3) return Container();

    return FutureBuilder<List<BlogNews>>(
      future: _getBlogsLayout,
      builder: (BuildContext context, AsyncSnapshot<List<BlogNews>> snapshot) {
        final locale = Provider.of<AppModel>(context).locale;
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.active:
          case ConnectionState.waiting:
            return Column(
              children: <Widget>[
                HeaderView(
                  headerText: widget.config["name"] != null
                      ? widget.config["name"][locale]
                      : '',
                  showSeeAll: isRecent ? false : true,
                  callback: () => BlogNews.showList(
                      context: context, config: widget.config),
                ),
                ConstrainedBox(
                  constraints: BoxConstraints(
                    minHeight: _buildProductHeight(screenSize.width, isTablet),
                  ),
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        SizedBox(width: 10.0),
                        for (var i = 0; i < 4; i++)
                          BlogCard(
                            item: BlogNews.empty(i),
                            width: _buildProductWidth(screenSize.width),
                          )
                      ],
                    ),
                  ),
                )
              ],
            );
          case ConnectionState.done:

          default:
            if (snapshot.hasError || snapshot.data.isEmpty) {
              return Container();
            } else {
              return Column(
                children: <Widget>[
                  HeaderView(
                    headerText: widget.config["name"] != null
                        ? widget.config["name"][locale]
                        : '',
                    showSeeAll: isRecent ? false : true,
                    callback: () => BlogNews.showList(
                        context: context,
                        config: widget.config,
                        blogs: isRecent ? recentProduct : snapshot.data),
                  ),
                  widget.config["layout"] == "staggered"
                      ? BlogStaggered(snapshot.data)
                      : getBlogsListWidgets(snapshot.data)
                ],
              );
            }
        }
      },
    );
  }
}
