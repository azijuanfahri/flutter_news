import 'dart:async';

import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class BlogWebView extends StatelessWidget {
  final String url;
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  BlogWebView({this.url});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(url),
        ),
        body: WebView(
          initialUrl: url,
          javascriptMode: JavascriptMode.unrestricted,
          onWebViewCreated: _controller.complete,
        ));
  }
}
