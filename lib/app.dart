import 'dart:async';
import 'dart:ui';

import 'package:after_layout/after_layout.dart';
import 'package:custom_splash/custom_splash.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flare_splash_screen/flare_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'common/config.dart';
import 'common/constants.dart';
import 'common/styles.dart';
import 'common/tools.dart';
import 'generated/i18n.dart';
import 'models/app.dart';
import 'models/blog_news.dart';
import 'models/category.dart';
import 'models/product.dart';
import 'models/recent_product.dart';
import 'models/search.dart';
import 'models/user.dart';
import 'models/wishlist.dart';
import 'screens/blogs.dart';
import 'screens/login.dart';
import 'screens/notification.dart';
import 'screens/onboard_screen.dart';
import 'screens/registration.dart';
import 'screens/wishlist.dart';
import 'services/wordpress.dart';
import 'tabbar.dart';

FirebaseAnalytics analytics = FirebaseAnalytics();

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle.dark
          .copyWith(statusBarColor: Theme.of(context).accentColor),
    );

    /// For Flare Image
    if (kSplashScreen.lastIndexOf('flr') > 0) {
      return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: SplashScreen.navigate(
          name: kSplashScreen,
          startAnimation: 'fluxstore',
          backgroundColor: Colors.white,
          next: (object) => MyApp(),
          until: () => Future.delayed(Duration(seconds: 2)),
        ),
      );
    }

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: CustomSplash(
        imagePath: kLogoImage,
        backGroundColor: Colors.white,
        animationEffect: 'fade-in',
        logoSize: 50,
        home: MyApp(),
        duration: 2500,
      ),
    );
  }
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AppState();
  }
}

class AppState extends State<MyApp> with AfterLayoutMixin {
  final _app = AppModel();
  final _product = ProductModel();
  final _wishlist = WishListModel();
  final _search = SearchModel();
  final _recent = RecentModel();
  final _blog = BlogNewsModel();
  bool isFirstSeen = false;
  bool isChecking = true;
  bool isLoggedIn = false;

  @override
  void afterFirstLayout(BuildContext context) async {
    WordPress().setAppConfig(serverConfig);
    await _app.loadAppConfig();

    isFirstSeen = await checkFirstSeen();
    isLoggedIn = await checkLogin();
    setState(() {
      isChecking = false;
    });
  }

  Future checkFirstSeen() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool _seen = (prefs.getBool('seen') ?? false);

    if (_seen) {
      return false;
    } else {
      await prefs.setBool('seen', true);
      return true;
    }
  }

  Future checkLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool('loggedIn') ?? false;
  }

  Widget renderFirstScreen() {
    if (isFirstSeen) return OnBoardScreen();
    if (kAdvanceConfig['IsRequiredLogin'] && !isLoggedIn) return LoginScreen();
    return MainTabs();
  }

  @override
  Widget build(BuildContext context) {
    if (isChecking) {
      return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          body: Container(),
        ),
      );
    }

    return ChangeNotifierProvider<AppModel>.value(
      value: _app,
      child: Consumer<AppModel>(
        builder: (context, value, child) {
          if (value.isLoading) {
            return Container(
              color: Colors.white,
            );
          }
          return MultiProvider(
            providers: [
              Provider<ProductModel>.value(value: _product),
              Provider<BlogNewsModel>.value(value: _blog),
              Provider<WishListModel>.value(value: _wishlist),
              Provider<SearchModel>.value(value: _search),
              Provider<RecentModel>.value(value: _recent),
              ChangeNotifierProvider(create: (_) => UserModel()),
              ChangeNotifierProvider(create: (_) => CategoryModel()),
              ChangeNotifierProvider(create: (_) => _blog)
            ],
            child: MaterialApp(
              debugShowCheckedModeBanner: false,
              locale: Locale(
                  Provider.of<AppModel>(context, listen: false).locale, ""),
              navigatorObservers: [
                FirebaseAnalyticsObserver(analytics: analytics),
              ],
              localizationsDelegates: [
                S.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
              ],
              supportedLocales: S.delegate.supportedLocales,
              localeListResolutionCallback:
                  S.delegate.listResolution(fallback: const Locale('en', '')),
              home: renderFirstScreen(),
              routes: <String, WidgetBuilder>{
                "/home": (context) => MainTabs(),
                "/login": (context) => LoginScreen(),
                "/register": (context) => RegistrationScreen(),
                '/wishlist': (context) => WishList(),
                '/blogs': (context) => BlogScreen(),
                '/notify': (context) => Notifications()
              },
              theme: Provider.of<AppModel>(context).darkTheme
                  ? buildDarkTheme().copyWith(
                      primaryColor:
                          HexColor(_app.appConfig["Setting"]["MainColor"]))
                  : buildLightTheme().copyWith(
                      primaryColor:
                          HexColor(_app.appConfig["Setting"]["MainColor"])),
            ),
          );
        },
      ),
    );
  }
}
