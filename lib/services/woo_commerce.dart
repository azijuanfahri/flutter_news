import 'dart:async';
import 'dart:convert' as convert;
import 'dart:convert';
import "dart:core";

import 'package:http/http.dart' as http;
import 'package:quiver/strings.dart';

import '../common/config.dart';
import '../models/aftership.dart';
import '../models/category.dart';
import '../models/coupon.dart';
import '../models/product.dart';
import '../models/review.dart';
import '../models/user.dart';
import 'helper/woocommerce_api.dart';
import 'index.dart';

class WooCommerce implements BaseServices {
  static final WooCommerce _instance = WooCommerce._internal();

  factory WooCommerce() => _instance;

  WooCommerce._internal();

  Map<String, dynamic> configCache;
  WooCommerceAPI wcApi;
  String isSecure;
  String url;
  List<Category> categories = [];

  void appConfig(appConfig) {
    wcApi = WooCommerceAPI(appConfig["url"], appConfig["consumerKey"],
        appConfig["consumerSecret"]);
    isSecure = appConfig["url"].indexOf('https') != -1 ? '' : '&insecure=cool';
    url = appConfig["url"];
  }

  Future<List<Category>> getCategoriesByPage({lang, page}) async {
    try {
      String url = "products/categories?exclude=311&per_page=100&page=$page";
      if (lang != null) {
        url += "&lang=$lang";
      }
      var response = await wcApi.getAsync(url);
      if (page == 1) {
        categories = [];
      }
      if (response is Map && isNotBlank(response["message"])) {
        throw Exception(response["message"]);
      } else {
        for (var item in response) {
          if (item['slug'] != "uncategorized" && item['count'] > 0) {
            categories.add(Category.fromJson(item));
          }
        }
        if (response.length == 100) {
          return getCategoriesByPage(lang: lang, page: page + 1);
        } else {
          return categories;
        }
      }
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<List<Category>> getCategories({lang}) async {
    try {
      List<Category> list = [];
      var cat =
          await wcApi.getAsync('products/categories?lang=$lang&per_page=100');
      for (var item in cat) {
        list.add(Category.fromJson(item));
      }
      print(list.length);
      return list;
    } catch (e) {
      return categories;
      //throw e;
    }
  }

  @override
  Future<List<Product>> getProducts() async {
    try {
      var response = await wcApi.getAsync("products");
      List<Product> list = [];
      if (response is Map && isNotBlank(response["message"])) {
        throw Exception(response["message"]);
      } else {
        for (var item in response) {
          list.add(Product.fromJson(item));
        }
        return list;
      }
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<List<Product>> fetchProductsLayout({config, lang}) async {
    try {
      List<Product> list = [];

      if (kAdvanceConfig['isCaching'] && configCache != null) {
        final horizontalLayout = configCache["HorizonLayout"] as List;
        var obj = horizontalLayout.firstWhere(
            (o) =>
                o["layout"] == config["layout"] &&
                (o["category"] == config["category"] ||
                    o["tag"] == config["tag"]),
            orElse: () => null);
        if (obj != null) return obj["data"];

        final verticalLayout = configCache["VerticalLayout"] as List;
        obj = verticalLayout.firstWhere(
            (o) =>
                o["layout"] == config["layout"] &&
                (o["category"] == config["category"] ||
                    o["tag"] == config["tag"]),
            orElse: () => null);
        if (obj != null) return obj["data"];
      }

      var endPoint = "products?lang=$lang&status=publish";
      if (config.containsKey("category") && config["category"] != null) {
        endPoint += "&category=${config["category"]}";
      }
      if (kAdvanceConfig['hideOutOfStock']) {
        endPoint += "&stockstatus=instock";
      }
      if (config.containsKey("tag") && config["tag"] != null) {
        endPoint += "&tag=${config["tag"]}";
      }
      if (config.containsKey("featured") && config["featured"] != null) {
        endPoint += "&featured=${config["featured"]}";
      }
      if (config.containsKey("page")) {
        endPoint += "&page=${config["page"]}";
      }
      if (config.containsKey("limit")) {
        endPoint += "&per_page=${config["limit"] ?? 10}";
      }

      var response = await wcApi.getAsync(endPoint);

      if (response is Map && isNotBlank(response["message"])) {
        throw Exception(response["message"]);
      } else {
        for (var item in response) {
          Product product = Product.fromJson(item);
          product.categoryId = config["category"];
          list.add(product);
        }
        return list;
      }
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      return [];
    }
  }

  @override
  Future<List<Product>> fetchProductsByCategory(
      {categoryId, page, minPrice, maxPrice, orderBy, lang, order}) async {
    try {
      List<Product> list = [];

      var endPoint =
          "products?status=publish&lang=$lang&per_page=10&page=$page";
      if (categoryId != null) {
        endPoint += "&category=$categoryId";
      }
      if (minPrice != null) {
        endPoint += "&min_price=${(minPrice as double).toInt().toString()}";
      }
      if (maxPrice != null && maxPrice > 0) {
        endPoint += "&max_price=${(maxPrice as double).toInt().toString()}";
      }
      if (orderBy != null) {
        endPoint += "&orderby=$orderBy";
      }
      if (order != null) {
        endPoint += "&order=$order";
      }
      print(endPoint);
      var response = await wcApi.getAsync(endPoint);

      if (response is Map && isNotBlank(response["message"])) {
        throw Exception(response["message"]);
      } else {
        for (var item in response) {
          list.add(Product.fromJson(item));
        }
        return list;
      }
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<User> loginFacebook({String token}) async {
    const cookieLifeTime = 120960000000;

    try {
      var endPoint = "$url/api/mstore_user/fb_connect/?second=$cookieLifeTime"
          "&access_token=$token$isSecure";

      var response = await http.get(endPoint);

      var jsonDecode = convert.jsonDecode(response.body);

      if (jsonDecode['status'] != 'ok') {
        return jsonDecode['msg'];
      }

      return User.fromJsonFB(jsonDecode);
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<User> loginSMS({String token}) async {
    try {
      //var endPoint = "$url/api/mstore_user/sms_login/?access_token=$token$isSecure";
      var endPoint =
          "$url/api/mstore_user/firebase_sms_login?phone=$token$isSecure";

      var response = await http.get(endPoint);

      var jsonDecode = convert.jsonDecode(response.body);

      return User.fromJsonSMS(jsonDecode);
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<User> loginApple({String email, String fullName}) async {
    try {
      var endPoint =
          "$url/api/mstore_user/apple_login?email=$email&display_name=$fullName&user_name=${email.split("@")[0]}$isSecure";

      var response = await http.get(endPoint);

      var jsonDecode = convert.jsonDecode(response.body);

      return User.fromJsonSMS(jsonDecode);
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<List<Review>> getReviews(productId) async {
    try {
      var response = await wcApi.getAsync("products/$productId/reviews", 2);
      List<Review> list = [];
      if (response is Map && isNotBlank(response["message"])) {
        throw Exception(response["message"]);
      } else {
        for (var item in response) {
          list.add(Review.fromJson(item));
        }
        return list;
      }
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<Null> createReview({int productId, Map<String, dynamic> data}) async {
    try {
      await wcApi.postAsync("products/$productId/reviews", data);
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<List<ProductVariation>> getProductVariations(Product product) async {
    try {
      var response =
          await wcApi.getAsync("products/${product.id}/variations?per_page=20");
      if (response is Map && isNotBlank(response["message"])) {
        throw Exception(response["message"]);
      } else {
        List<ProductVariation> list = [];
        for (var item in response) {
          list.add(ProductVariation.fromJson(item));
        }
        return list;
      }
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<List<Product>> searchProducts({name, page}) async {
    try {
      var response = await wcApi.getAsync(
          "products?status=publish&search=$name&page=$page&per_page=50");
      if (response is Map && isNotBlank(response["message"])) {
        throw Exception(response["message"]);
      } else {
        List<Product> list = [];
        for (var item in response) {
          list.add(Product.fromJson(item));
        }
        return list;
      }
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  /// Get Nonce for Any Action
  Future getNonce({method = 'register'}) async {
    try {
      http.Response response = await http.get(
          "$url/api/get_nonce/?controller=mstore_user&method=$method&$isSecure");
      if (response.statusCode == 200) {
        return convert.jsonDecode(response.body)['nonce'];
      } else {
        throw Exception(['error getNonce', response.statusCode]);
      }
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  /// Auth
  @override
  Future<User> getUserInfo(cookie) async {
    try {
//      print("$url/api/mstore_user/get_currentuserinfo/?cookie=$cookie&$isSecure");

      final http.Response response = await http.get(
          "$url/api/mstore_user/get_currentuserinfo/?cookie=$cookie&$isSecure");
      if (response.statusCode == 200) {
        return User.fromAuthUser(
            convert.jsonDecode(response.body)['user'], cookie);
      } else {
        throw Exception("Can not get user info");
      }
    } catch (err) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  /// Create a New User
  @override
  Future<User> createUser({firstName, lastName, username, password}) async {
    try {
      String niceName = firstName + " " + lastName;
      var nonce = await getNonce();
      final http.Response response = await http.post(
          "$url/api/mstore_user/register/?insecure=cool&$isSecure",
          body: convert.jsonEncode({
            "nonce": nonce,
            "user_email": username,
            "user_login": username,
            "username": username,
            "user_pass": password,
            "email": username,
            "user_nicename": niceName,
            "display_name": niceName,
          }));
      var body = convert.jsonDecode(response.body);
      if (response.statusCode == 200 && body["error"] == null) {
        var cookie = body['cookie'];
        return await this.getUserInfo(cookie);
      } else {
        var message = body["error"];
        throw Exception(message != null ? message : "Can not create the user.");
      }
    } catch (err) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  /// login
  @override
  Future<User> login({username, password}) async {
    var cookieLifeTime = 120960000000;
    try {
      final http.Response response = await http.post(
          "$url/api/mstore_user/generate_auth_cookie/?insecure=cool&$isSecure",
          body: convert.jsonEncode({
            "seconds": cookieLifeTime.toString(),
            "username": username,
            "password": password
          }));

      final body = convert.jsonDecode(response.body);
      if (response.statusCode == 200 && isNotBlank(body['cookie'])) {
        return await this.getUserInfo(body['cookie']);
      } else {
        throw Exception("The username or password is incorrect.");
      }
    } catch (err) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  Future<Stream<Product>> streamProductsLayout({config}) async {
    try {
      var endPoint = "products?per_page=10";
      if (config.containsKey("category")) {
        endPoint += "&category=${config["category"]}";
      }
      if (config.containsKey("tag")) {
        endPoint += "&tag=${config["tag"]}";
      }

      http.StreamedResponse response = await wcApi.getStream(endPoint);

      return response.stream
          .transform(utf8.decoder)
          .transform(json.decoder)
          .expand((data) => (data as List))
          .map((data) => Product.fromJson(data));
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<Product> getProduct(id) async {
    try {
      var response = await wcApi.getAsync("products/$id");
      return Product.fromJson(response);
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<Coupons> getCoupons() async {
    try {
      var response = await wcApi.getAsync("coupons");
      //print(response.toString());
      return Coupons.getListCoupons(response);
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<AfterShip> getAllTracking() async {
    final data = await http.get('https://api.aftership.com/v4/trackings',
        headers: {'aftership-api-key': afterShip['api']});
    return AfterShip.fromJson(json.decode(data.body));
  }

  @override
  Future<User> getUserInfor({int id}) async {
    try {
      var response = await wcApi.getAsync('customers/${id.toString()}');
      if (response is Map && isNotBlank(response["message"])) {
        throw Exception(response["message"]);
      } else {
        return User.fromWoJson(response);
      }
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  Future<Map<String, dynamic>> getHomeCache() async {
    try {
      final data = await http.get('$url/wp-json/mstore/v1/cache');
      var config = json.decode(data.body);
      if (data.statusCode == 200 && config['HorizonLayout'] != null) {
        var horizontalLayout = config['HorizonLayout'] as List;
        var items = [];
        var products = [];
        List<Product> list;
        for (var i = 0; i < horizontalLayout.length; i++) {
          if (horizontalLayout[i]["radius"] != null) {
            horizontalLayout[i]["radius"] =
                double.parse("${horizontalLayout[i]["radius"]}");
          }
          if (horizontalLayout[i]["size"] != null) {
            horizontalLayout[i]["size"] =
                double.parse("${horizontalLayout[i]["size"]}");
          }
          if (horizontalLayout[i]["padding"] != null) {
            horizontalLayout[i]["padding"] =
                double.parse("${horizontalLayout[i]["padding"]}");
          }

          products = horizontalLayout[i]["data"] as List;
          list = [];
          if (products != null && products.isNotEmpty) {
            for (var item in products) {
              Product product = Product.fromJson(item);
              product.categoryId = horizontalLayout[i]["category"];
              list.add(product);
            }
            horizontalLayout[i]["data"] = list;
          }

          items = horizontalLayout[i]["items"] as List;
          if (items != null && items.isNotEmpty) {
            for (var j = 0; j < items.length; j++) {
              if (items[j]["padding"] != null) {
                items[j]["padding"] = double.parse("${items[j]["padding"]}");
              }

              List<Product> listProduct = [];
              var prods = items[j]["data"] as List;
              if (prods != null && prods.isNotEmpty) {
                for (var prod in prods) {
                  listProduct.add(Product.fromJson(prod));
                }
                items[j]["data"] = listProduct;
              }
            }
          }
        }

        configCache = config;
        return config;
      }
      return null;
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<User> loginGoogle({String token}) async {
    const cookieLifeTime = 120960000000;

    try {
      var endPoint = "$url/api/mstore_user/google_login/?second=$cookieLifeTime"
          "&access_token=$token$isSecure";

      var response = await http.get(endPoint);

      var jsonDecode = convert.jsonDecode(response.body);

      if (jsonDecode['status'] != 'ok') {
        return jsonDecode['error'];
      }

      return User.fromJsonFB(jsonDecode);
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }
}
