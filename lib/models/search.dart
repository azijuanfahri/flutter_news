import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../common/constants.dart';
import '../models/blog_news.dart';
import '../services/index.dart';
import '../services/wordpress.dart';
import 'product.dart';

class SearchModel extends ChangeNotifier {
  SearchModel() {
    getKeywords();
  }

  List<String> keywords = [];
  List<Product> products = [];
  List<BlogNews> blogs = [];
  bool loading = false;
  String errMsg;

  void searchBlogs({String name}) async {
    try {
      loading = true;
      notifyListeners();
      blogs = await WordPress.searchBlog(name: name);
      if (blogs.isNotEmpty && name.isNotEmpty) {
        int index = keywords.indexOf(name);
        if (index > -1) {
          keywords.removeAt(index);
        }
        keywords.insert(0, name);
        saveKeywords(keywords);
      }
      loading = false;
      notifyListeners();
    } catch (err) {
      loading = false;
      errMsg = err.toString();
      notifyListeners();
    }
  }

  searchProducts({String name, page}) async {
    try {
      loading = true;
      notifyListeners();
      products = await Services().searchProducts(name: name, page: page);
      if (products.isNotEmpty && page == 1 && name.isNotEmpty) {
        int index = keywords.indexOf(name);
        if (index > -1) {
          keywords.removeAt(index);
        }
        keywords.insert(0, name);
        saveKeywords(keywords);
      }
      loading = false;
      notifyListeners();
    } catch (err) {
      loading = false;
      errMsg = err.toString();
      notifyListeners();
    }
  }

  void clearKeywords() {
    keywords = [];
    saveKeywords(keywords);
    notifyListeners();
  }

  void saveKeywords(List<String> keywords) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setStringList(kLocalKey["recentSearches"], keywords);
    } catch (err) {
      print(err);
    }
  }

  void getKeywords() async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      final list = prefs.getStringList(kLocalKey["recentSearches"]);
      if (list != null && list.isNotEmpty) {
        keywords = list;
      }
    } catch (err) {
//      print(err);
    }
  }
}
